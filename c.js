import React, { Component } from 'react';

const withHOC = (PassedComponent) => {
  class withHOCClass extends Component {
    render() {
      return (
        <div>
          <PassedComponent />
        </div>
      )
    }
  }
  return withHOCClass;
}

const Print =  () =>{
  return (
    <div>C Print</div>
  )
}

export { Print as P };

export default withHOC(Print);
