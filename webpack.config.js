const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const env = "production"

const plugins = [];

module.exports = {
  entry: {
    app: './index.js',
    vendor: [
      'react',
      'react-router',
    ]
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'js/app.[hash].js',
    chunkFilename: '[name].[hash].js',
    sourceMapFilename: 'sourcemaps/[file].map',
    publicPath:'/'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules\/(?!(cleartax-shared-fe)\/).*/,
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: plugins.concat([
    // new LodashModuleReplacementPlugin({
    //   collections: true,
    //   paths: true,
    // }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    // new webpack.ProvidePlugin({
    //   Promise: 'imports-loader?this=>global!exports-loader?global.Promise!es6-promise',
    //   fetch: 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch',
    // }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendor.[hash].js',
      minChunks(module) {
        const context = module.context;
        if (typeof context !== 'string') return false;
        return context.indexOf('node_modules') !== -1 &&
          context.indexOf('@cleartax') === -1 &&
          context.indexOf('pdfmake') === -1;
      },
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || env),
        BUILD_ENV: JSON.stringify(env),
        APP_ENV: JSON.stringify('browser'),
        NODE_FETCH_TOKEN: JSON.stringify('true'),
        CONFIG: JSON.stringify({}),
      },
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: false,
      __DEVTOOLS__: false,
    }),
    // new Extract('css/bundle.[contenthash:6].css', {
    //   allChunks: true,
    // }),
    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: 'body',
      environment: env,
    }),
    new webpack.ExtendedAPIPlugin(),
  ]),
};

