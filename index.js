import React, { Children } from 'react';
import { render } from 'react-dom'

import { Router, Route, browserHistory, IndexRoute, Link } from 'react-router';

const Parent = ({children}) =>{
  return (
    <div>
      <ul>
        <li><Link to="/">Index</Link></li>
        <li><Link to="/a">A</Link></li>
        <li><Link to="/b">B</Link></li>
        <li><Link to="/c">C</Link></li>
        <li><Link to="/d">D</Link></li>
      </ul>
      {children}
    </div>
  )
}

const App = () => {
  return (
    <Router history={browserHistory}>
      <Route path="/" name="root" component={Parent}>
        <IndexRoute  getComponent={(nextState, callback) => {
              import(/* webpackChunkName: "a" */ './a')
                .then((module) => {
                  console.log(module);
                  callback(null, module.default);
                });
            }} />/>
        <Route path="a" name="A" getComponent={(nextState, callback) => {
              import(/* webpackChunkName: "a" */ './a')
                .then((module) => {
                  console.log(module);
                  callback(null, module.default);
                });
            }} />
        <Route path="b" name="B" getComponent={(nextState, callback) => {
              import(/* webpackChunkName: "b" */ './b')
                .then((module) => {
                  console.log(module);
                  callback(null, module.default);
                });
            }} />
        <Route path="c" name="C" getComponent={(nextState, callback) => {
              import(/* webpackChunkName: "c" */ './c')
                .then((module) => {
                  console.log(module);
                  callback(null, module.default);
                });
            }} />
        <Route path="d" name="D" getComponent={(nextState, callback) => {
              import(/* webpackChunkName: "a" */ './a')
                .then((module) => {
                  console.log(module);
                  callback(null, module.default);
                });
            }} />
      </Route>
    </Router>
  )
}

window.addEventListener('DOMContentLoaded', () => {
  const appName = "app";
  let container = document.getElementById(appName);

  if (!container) {
    container = document.createElement('div');
    container.id = appName;
    container.className = appName;
    document.body.appendChild(container);
}

  render(<App />, container);

})
